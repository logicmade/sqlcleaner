DECLARE @oid INT
DECLARE @TName NVARCHAR(50)

DECLARE MyCursor CURSOR FOR SELECT A.name, A.object_id FROM sys.tables A
OPEN MyCursor

FETCH NEXT FROM MyCursor INTO @TName, @oid
WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE @ColName NVARCHAR(50)
	DECLARE @ColType NVARCHAR(50)

	DECLARE Cur2 CURSOR FOR
	SELECT col.name, typ.name  FROM  sys.columns col 
			 JOIN sys.types typ ON col.system_type_id = typ.system_type_id 
			 AND col.user_type_id = typ.user_type_id 
			 WHERE object_id = @oid;
	OPEN Cur2
	FETCH NEXT FROM Cur2 INTO @ColName, @ColType
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (@ColType = 'nvarchar' OR @ColType = 'varchar' OR @ColType = 'char' OR @ColType = 'text' OR @ColType = 'ntext' OR @ColType = 'nchar' )   
		BEGIN
			DECLARE @SQL NVARCHAR(MAX)
			SET @SQL = 'UPDATE ' + @TName + ' SET  ' + @ColName + ' = (CASE WHEN CHARINDEX(''</title'', ' + @ColName + ') > 0 THEN SUBSTRING(' + @ColName + ',0, CHARINDEX(''</title'', ' + @ColName + '))  ELSE ' + @ColName + ' END)'
			EXEC(@SQL)
		--PRINT @SQL
		END
		FETCH NEXT FROM Cur2 INTO @ColName, @ColType
	END;
	CLOSE Cur2;
	DEALLOCATE Cur2;

   FETCH NEXT FROM MyCursor INTO @TName, @oid
END
CLOSE MyCursor
DEALLOCATE MyCursor

